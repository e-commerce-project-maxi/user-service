insert into back_office.role (id, activity, create_date, name)
values  (1, true, '2022-05-18 11:11:28.000000', 'SUPER_ADMIN'),
        (2, true, '2022-05-18 11:11:31.000000', 'ADMIN'),
        (3, true, '2022-05-18 11:11:30.000000', 'MANAGER'),
        (4, true, '2022-05-18 11:11:33.000000', 'USER');


insert into back_office.permission (id, activity, create_date, name)
values  (1, true, '2022-05-18 11:11:59.000000', 'CREATE'),
        (2, true, '2022-05-18 11:12:01.000000', 'READ'),
        (3, true, '2022-05-18 11:12:01.000000', 'UPDATE'),
        (4, true, '2022-05-18 11:12:02.000000', 'DELETE');

insert into back_office.role_permission (id, permission_id, role_id)
values  (1, 1, 1),
        (2, 2, 1),
        (3, 3, 1),
        (4, 4, 1),
        (5, 1, 2),
        (6, 2, 2),
        (7, 3, 2),
        (8, 1, 3),
        (9, 2, 3),
        (10, 2, 4);

insert into back_office.admin_tool_menu (id, created_date, is_active, parent_id, tool_name)
values  (1, '2022-05-18 11:14:56.000000', true, 0, 'USER_SERVICE'),
        (2, '2022-05-18 11:16:23.000000', true, 0, 'CUSTOMER_SERVICE'),
        (3, '2022-05-18 11:16:47.000000', true, 0, 'AGENT_SERVICE');
