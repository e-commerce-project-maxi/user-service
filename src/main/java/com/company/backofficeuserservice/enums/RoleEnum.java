package com.company.backofficeuserservice.enums;

public enum RoleEnum {
    ADMIN(1,"Admin"),
    MANAGER(2,"Manager"),
    USER(3,"User"),
    SUPER_ADMIN(4,"Super Admin");


    private int id;
    private String roleName;

    RoleEnum(int id, String roleName) {
        this.id = id;
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public int getId() {
        return id;
    }
}
