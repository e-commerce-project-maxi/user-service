package com.company.backofficeuserservice.enums;

import org.springframework.http.HttpStatus;

public enum BusinessExceptionEnum {

    USER_NOT_FOUND("User not found by id: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user",
            "Error happened while getting user"),
    USER_NOT_FOUND_BY_EMAIL("User not found by email: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user by email",
            "Error happened while getting user by email"),
    USER_NOT_FOUND_BY_USERNAME("User not found by username: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user by username",
            "Error happened while getting user by username"),
    PASSWORD_NOT_MATCH("Password not matched",
            HttpStatus.BAD_REQUEST,
            "Password not matched",
            "Password not matched"),
    MESSAGE_ERROR("Error happened while send message",
            HttpStatus.BAD_REQUEST,
            "Error happened while send message",
            "Error happened while send message"),
    EMAIL_ALREADY_EXIST("Email %s is already exist",
            HttpStatus.BAD_REQUEST,
            "Email %s is already exist",
            "Email %s is already exist"),
    UN_SUCCESSFUL_OPERATION("Change status operation failed",
            HttpStatus.BAD_REQUEST,
            "Change status operation failed",
            "Change status operation failed"),
    ADMIN_TOOL_MENU_NOT_FOUND("Admin tool menu not found by id %s",
            HttpStatus.BAD_REQUEST,
            "Admin tool menu not found",
            "Admin tool menu not found"),
    ROLE_NOT_FOUND("Role not found by id %s",
            HttpStatus.BAD_REQUEST,
            "Role not found by id",
            "Role not found by id"),
    TOKEN_NOT_VALID("Token not valid",
            HttpStatus.BAD_REQUEST,
            "Token not valid",
            "Token not valid");

    private final String message;
    private final HttpStatus code;
    private final String reason;
    private final String description;

    BusinessExceptionEnum(String message, HttpStatus code, String reason, String description) {
        this.message = message;
        this.code = code;
        this.reason = reason;
        this.description = description;
    }

    public String getMessage(Object... params) {
        return String.format(this.message, params);
    }

    public String getCode() {
        return this.code.toString();
    }

    public String getReason(Object... params) {
        return String.format(this.reason, params);
    }

    public String getDescription(Object... params) {
        return String.format(this.description, params);
    }
}
