package com.company.backofficeuserservice.enums;

public enum ApiExceptionTypeEnum {
    RESOURCE_NOT_FOUND("RESOURCE_NOT_FOUND", "Resource (%s) is not found.", "Resource identification [%s] is failed.");

    private String code;
    private String description;
    private String reason;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getReason() {
        return reason;
    }

    ApiExceptionTypeEnum(String code, String description, String reason) {
        this.code = code;
        this.description = description;
        this.reason = reason;
    }
}
