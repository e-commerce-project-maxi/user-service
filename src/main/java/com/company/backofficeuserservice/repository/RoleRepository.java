package com.company.backofficeuserservice.repository;

import com.company.backofficeuserservice.entity.staticData.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Short> {
    boolean existsRoleById(Short roleId);
}
