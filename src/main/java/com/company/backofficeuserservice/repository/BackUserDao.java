//package com.company.backofficeuserservice.repository;
//
//import com.company.backofficeuserservice.enums.RoleEnum;
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.EntityManager;
//import javax.persistence.TypedQuery;
//
//@Repository
//@RequiredArgsConstructor
//public class BackUserDao {
//
//    private final EntityManager entityManager;
//
//    public boolean checkAccess(String username, int adminToolMenuId, int operation) {
//        TypedQuery<String> tp = entityManager.createQuery(
//                "select r.name " +
//                        "from BackUser b " +
//                        "inner join UserMenuJoin umj on umj.backUserId=b.id " +
//                        "inner join Role r on r.id = umj.roleId " +
//                        "where b.username=: username and umj.adminToolMenuId=:adminToolMenuId"
//                , String.class);
//        tp.setParameter("username", username);
//        tp.setParameter("adminToolMenuId", adminToolMenuId);
//
//        String role = tp.getSingleResult();
//        if (operation == 1 && RoleEnum.ADMIN.name().equals(role)) {
//            return true;
//        }
//        return false;
//    }
//}
