package com.company.backofficeuserservice.repository;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.entity.UserMenuJoin;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMenuJoinRepository extends JpaRepository<UserMenuJoin, Long> {
    UserMenuJoin findUserMenuJoinByAdminToolMenu_IdAndBackUser_Id(Integer adminToolId,Long backUserId);
}
