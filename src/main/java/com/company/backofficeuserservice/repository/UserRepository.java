package com.company.backofficeuserservice.repository;

import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.enums.UserStatus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<BackUser, Long> {
    Optional<BackUser> findBackUserByUsername(String username);

    Optional<BackUser> findBackUserByEmail(String email);

    boolean existsBackUserByEmail(String email);

    boolean existsBackUserById(Long id);

    List<BackUser> findAllByStatus(Integer status);

}
