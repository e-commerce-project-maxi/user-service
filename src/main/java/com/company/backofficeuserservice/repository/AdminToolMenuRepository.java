package com.company.backofficeuserservice.repository;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminToolMenuRepository extends JpaRepository<AdminToolMenu,Integer> {

    boolean existsAdminToolMenuById(Integer id);
}
