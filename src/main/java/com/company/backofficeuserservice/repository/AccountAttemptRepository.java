package com.company.backofficeuserservice.repository;

import com.company.backofficeuserservice.entity.AccountAttempt;
import com.company.backofficeuserservice.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AccountAttemptRepository extends JpaRepository<AccountAttempt, Long> {

    AccountAttempt findByUsername(String username);
}
