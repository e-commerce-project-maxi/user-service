package com.company.backofficeuserservice.util;

import com.company.backofficeuserservice.container.BCrypt;

public class PasswordCryptChecking {

    public static boolean matchPassword(String password, String originalPassword) {
        return BCrypt.checkpw(password, originalPassword);
    }

    public static String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(12));
    }
}
