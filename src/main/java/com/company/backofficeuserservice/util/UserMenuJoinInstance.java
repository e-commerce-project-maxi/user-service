package com.company.backofficeuserservice.util;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.entity.RolePermission;
import com.company.backofficeuserservice.entity.UserMenuJoin;
import com.company.backofficeuserservice.entity.staticData.Role;
import org.springframework.stereotype.Component;

import static com.company.backofficeuserservice.constant.CommonConstant.DEFAULT_ADMIN_TOOL_MENU_ID;
import static com.company.backofficeuserservice.constant.CommonConstant.DEFAULT_ROLE_ID;

@Component
public class UserMenuJoinInstance {

    public UserMenuJoin createInstance(BackUser backUser, AdminToolMenu adminToolMenu, Role role) {
        UserMenuJoin userMenuJoin = new UserMenuJoin();
        userMenuJoin.setBackUser(backUser);
        userMenuJoin.setAdminToolMenu(adminToolMenu);
        userMenuJoin.setRole(role);
        return userMenuJoin;
    }


}
