package com.company.backofficeuserservice.util;

import static com.company.backofficeuserservice.constant.DefaultUsernameAndPassword.DEFAULT_USERNAME;

public class UsernameTemporaryGenerator {
    public static String generateUsername() {
        return DEFAULT_USERNAME + getNano();
    }
    private static String getNano() {
        return String.valueOf(System.nanoTime()).substring(10);
    }
}
