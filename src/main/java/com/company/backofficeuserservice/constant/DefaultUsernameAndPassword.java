package com.company.backofficeuserservice.constant;


public interface DefaultUsernameAndPassword {

    String DEFAULT_USERNAME = "admin_";
    // 1234 password
    String DEFAULT_PASSWORD = "$2a$12$h3xuysJ9mrM2WWNR0w4fOehV7X.93if5V49HJuPEHv3j98O5Ysulq";
}
