package com.company.backofficeuserservice.constant;

public interface CommonConstant {
    int DEFAULT_ADMIN_TOOL_MENU_ID = 1;
    short DEFAULT_ROLE_ID = 4;
    String SUCCESS = "Success";
    String CONTINUE_OPERATION = "Continue Operation";
    String SECURITY_HEADER = "Authorization";
    interface EmailConstant {
        String SUBJECT_LINE = "This is temporary password";
        String MESSAGE_TEXT = "PASSWORD : %s";
        String MAIL_MESSAGE = "Password send to your mail.Please check mail box";
    }

    interface ValidationConstant {
        String ADMIN_TOOL_MENU_VERIFY = "adminToolMenuVerify";
        String ROLE_VERIFY = "roleVerify";
        String USER_VERIFY = "userVerify";
    }
}
