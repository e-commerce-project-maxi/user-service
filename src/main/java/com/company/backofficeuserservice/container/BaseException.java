package com.company.backofficeuserservice.container;

public interface BaseException {

    String getCode();

    String getDescription();
}
