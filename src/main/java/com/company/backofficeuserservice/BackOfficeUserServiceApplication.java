package com.company.backofficeuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackOfficeUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackOfficeUserServiceApplication.class, args);
    }

}
