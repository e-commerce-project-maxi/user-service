package com.company.backofficeuserservice.exception;

import com.company.backofficeuserservice.enums.ApiExceptionTypeEnum;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;

public class TokenNotValidException extends BaseApiException{
    private static final long serialVersionUID = -5271999716427034304L;

    public TokenNotValidException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public TokenNotValidException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
