package com.company.backofficeuserservice.exception;

import com.company.backofficeuserservice.enums.ApiExceptionTypeEnum;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;

public class PasswordException extends BaseApiException{
    private static final long serialVersionUID = -2232928862067245707L;

    public PasswordException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public PasswordException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
