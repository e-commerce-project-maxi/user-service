package com.company.backofficeuserservice.exception;

import com.company.backofficeuserservice.enums.ApiExceptionTypeEnum;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;

public class NotFoundException extends BaseApiException {
    private static final long serialVersionUID = 6009574796350506068L;

    public NotFoundException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public NotFoundException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
