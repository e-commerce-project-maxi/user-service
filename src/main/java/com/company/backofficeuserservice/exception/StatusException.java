package com.company.backofficeuserservice.exception;

import com.company.backofficeuserservice.enums.ApiExceptionTypeEnum;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;

public class StatusException  extends BaseApiException{

    private static final long serialVersionUID = 8668146451860157332L;

    public StatusException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public StatusException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
