package com.company.backofficeuserservice.dto.response.user;

import com.company.backofficeuserservice.dto.BaseDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BackUserResponse implements BaseDto {
    Long id;
    String firstName;
    String lastName;
    String username;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime createdDate;
    Integer statusId;
    String email;
}
