package com.company.backofficeuserservice.dto.reuqest.user;

import com.company.backofficeuserservice.validation.annotation.ValidPassword;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChangePasswordRequest {
    String username;
    String oldPassword;
    @ValidPassword
    String newPassword;
}
