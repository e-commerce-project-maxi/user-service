package com.company.backofficeuserservice.dto.reuqest.validationDto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ValidationRequest implements Serializable {
    private Long userId;
    private Integer adminMenuId;
    private Short roleId;
    private String username;
}
