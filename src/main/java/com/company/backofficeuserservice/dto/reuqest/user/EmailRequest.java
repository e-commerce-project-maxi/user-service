package com.company.backofficeuserservice.dto.reuqest.user;

import com.company.backofficeuserservice.dto.BaseDto;
import com.company.backofficeuserservice.validation.annotation.Email;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailRequest implements BaseDto {
    @Email
    String email;
}
