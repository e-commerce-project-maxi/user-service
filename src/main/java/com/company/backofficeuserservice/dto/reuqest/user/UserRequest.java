package com.company.backofficeuserservice.dto.reuqest.user;

import com.company.backofficeuserservice.dto.BaseDto;
import com.company.backofficeuserservice.validation.annotation.CharacterValidation;
import com.company.backofficeuserservice.validation.annotation.Email;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest implements BaseDto {
    String username;
    @CharacterValidation
    String firstName;
    @CharacterValidation
    String lastName;
    @Email
    String email;
}
