package com.company.backofficeuserservice.controller;

import com.company.backofficeuserservice.container.CrossSafeRestResource;
import com.company.backofficeuserservice.container.api.ApiBuilder;
import com.company.backofficeuserservice.container.api.CollectionMessage;
import com.company.backofficeuserservice.container.api.SingleMessage;
import com.company.backofficeuserservice.converter.RequestDtoConverter;
import com.company.backofficeuserservice.dto.response.user.BackUserResponse;
import com.company.backofficeuserservice.dto.reuqest.user.ChangePasswordRequest;
import com.company.backofficeuserservice.dto.reuqest.user.EmailRequest;
import com.company.backofficeuserservice.dto.reuqest.user.UserRequest;
import com.company.backofficeuserservice.enums.UserStatus;
import com.company.backofficeuserservice.service.businessService.BackUserBusinessService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RequiredArgsConstructor
@CrossSafeRestResource(path = "/user")
public class UserController implements ApiBuilder {

    private final BackUserBusinessService backUserService;
    private final RequestDtoConverter requestDtoConverter;

    @PostMapping
    public ResponseEntity<SingleMessage<BackUserResponse>> create(@RequestBody @Valid UserRequest userRequest) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.createUser(userRequest)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SingleMessage<BackUserResponse>> update(
            @PathVariable Long id,
            @Valid @RequestBody UserRequest userRequest) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.updateUser(id, userRequest)));
    }

    @PutMapping("/{id}/role")
    public ResponseEntity<SingleMessage<String>> updateRole(
            @PathVariable Long id,
            @RequestParam int menuId,
            @RequestParam short roleId) {
        backUserService.updateUserRoleBaseAdminToolMenu(id, menuId, roleId);
        return ResponseEntity.ok(generateSingleMessage("Success"));
    }

    @PutMapping("/{id}/add-admin-tool")
    public ResponseEntity<SingleMessage<String>> addAdminTool(
            @PathVariable Long id,
            @RequestParam int menuId,
            @RequestParam short roleId) {
        backUserService.addAdminToolMenuAndRoleForUser(id, menuId, roleId);
        return ResponseEntity.ok(generateSingleMessage("Success"));
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<SingleMessage<String>> updateStatus(@PathVariable Long id,
                                                              @RequestParam UserStatus userStatus) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.updateStatus(id, userStatus)));
    }

    @GetMapping
    public ResponseEntity<CollectionMessage<BackUserResponse>> getAll(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size) {
        return ResponseEntity.ok(generateCollectionMessage(backUserService.findAll(page, size)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<SingleMessage<BackUserResponse>> getById(@PathVariable Long id) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.findById(id)));
    }

    @GetMapping("by-username/{username}")
    public ResponseEntity<SingleMessage<BackUserResponse>> getByUsername(@PathVariable String username) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.findByUserName(username)));
    }

    @GetMapping("/generate-password")
    public ResponseEntity<SingleMessage<String>> generatePassword(@RequestBody @Valid EmailRequest request) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.generatePassword(request)));
    }

    //TODO username i token ile alacaq
    @PostMapping("/change-password")
    public ResponseEntity<SingleMessage<BackUserResponse>> changePassword(
            @RequestBody @Valid ChangePasswordRequest changePasswordRequest) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.changePassword(changePasswordRequest)));
    }

    @GetMapping("/check-email")
    public ResponseEntity<SingleMessage<String>> checkEmail(@RequestBody @Valid EmailRequest request) {
        System.out.println("");
        return ResponseEntity.ok(generateSingleMessage(backUserService.checkEmailAlreadyExist(request)));
    }

    @PatchMapping("/un-block/{username}")
    public ResponseEntity<SingleMessage<String>> unBlockUser(@PathVariable String username) {
        return ResponseEntity.ok(generateSingleMessage(backUserService.unBlockUser(username)));
    }
}
