package com.company.backofficeuserservice.mapper;

import com.company.backofficeuserservice.dto.response.user.BackUserResponse;
import com.company.backofficeuserservice.dto.reuqest.user.UpdateRequest;
import com.company.backofficeuserservice.dto.reuqest.user.UserRequest;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.mapper.decorator.UserDecorator;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
@DecoratedWith(UserDecorator.class)
public interface UserMapper extends BaseMapper<UserRequest, BackUser, BackUserResponse> {

    @Override
    BackUserResponse toDto(BackUser backUser);

    @Override
    BackUser toEntity(UserRequest userRequest);

    @Override
    BackUser toCreateEntity(UserRequest userRequest);

    @Override
    BackUser createInstance(@MappingTarget BackUser backUser, UserRequest userRequest);
}
