package com.company.backofficeuserservice.mapper;

public interface BaseMapper<REQ, ENT, RES> {

    RES toDto(ENT ent);

    ENT toEntity(REQ req);

    ENT toCreateEntity(REQ req);

    ENT createInstance(ENT ent,REQ req);
}
