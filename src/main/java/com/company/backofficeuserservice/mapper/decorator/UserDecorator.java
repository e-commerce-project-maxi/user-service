package com.company.backofficeuserservice.mapper.decorator;

import com.company.backofficeuserservice.dto.response.user.BackUserResponse;
import com.company.backofficeuserservice.dto.reuqest.user.UserRequest;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.enums.UserStatus;
import com.company.backofficeuserservice.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.time.LocalDateTime;

import static com.company.backofficeuserservice.constant.DefaultUsernameAndPassword.DEFAULT_PASSWORD;
import static com.company.backofficeuserservice.util.UsernameTemporaryGenerator.generateUsername;

@RequiredArgsConstructor
public class UserDecorator implements UserMapper {

    private UserMapper userMapper;

    @Autowired
    @Qualifier("delegate")
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public BackUserResponse toDto(BackUser backUser) {
        BackUserResponse userResponse = userMapper.toDto(backUser);
        userResponse.setStatusId(backUser.getStatus().getId());
        return userResponse;
    }

    @Override
    public BackUser toEntity(UserRequest userRequest) {
        return userMapper.toEntity(userRequest);
    }

    @Override
    public BackUser toCreateEntity(UserRequest userRequest) {
        BackUser backUser = userMapper.toCreateEntity(userRequest);
        backUser.setStatus(UserStatus.IN_ACTIVE);
        backUser.setCreatedDate(LocalDateTime.now());
        backUser.setUsername(generateUsername());
        backUser.setPassword(DEFAULT_PASSWORD);
        return backUser;
    }

    @Override
    public BackUser createInstance(BackUser backUser, UserRequest userRequest) {
        backUser.setUsername(userRequest.getUsername());
        backUser.setFirstName(userRequest.getFirstName());
        backUser.setLastName(userRequest.getLastName());
        backUser.setEmail(userRequest.getEmail());
        return backUser;
    }
}
