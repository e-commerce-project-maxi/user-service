package com.company.backofficeuserservice.entity;

import com.company.backofficeuserservice.entity.staticData.Permission;
import com.company.backofficeuserservice.entity.staticData.Role;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(schema = "back_office")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RolePermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Short id;
    @OneToOne
    Role role;
    @OneToOne
    Permission permission;
}
