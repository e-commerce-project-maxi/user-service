package com.company.backofficeuserservice.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(schema = "back_office")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminToolMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "tool_name", nullable = false, length = 50)
    String toolName;
    long parentId = 0;
    boolean isActive;
    LocalDateTime createdDate;
}
