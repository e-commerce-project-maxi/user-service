package com.company.backofficeuserservice.entity;

import com.company.backofficeuserservice.enums.UserStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(schema = "back_office")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BackUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String firstName;
    String lastName;
    @Column(unique = true)
    String username;
    LocalDateTime createdDate;
    LocalDateTime updateDate;
    private LocalDateTime passwordExpiredTime;
    String password;
    @Enumerated(EnumType.STRING)
    UserStatus status;
    @Column(unique = true)
    String email;
    boolean accountBlock;
}
