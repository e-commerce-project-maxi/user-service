package com.company.backofficeuserservice.entity;

import com.company.backofficeuserservice.entity.staticData.Role;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(schema = "back_office")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserMenuJoin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    private BackUser backUser;
    @ManyToOne
    private Role role;
    @ManyToOne
    private AdminToolMenu adminToolMenu;
}
