package com.company.backofficeuserservice.entity;

import com.company.backofficeuserservice.enums.Status;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(schema = "back_office")
public class AccountAttempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    int attemptCount;
    Status status;
    String username;
    Long userId;
    LocalDateTime creationTime;
    LocalDateTime modifiedTime;
}
