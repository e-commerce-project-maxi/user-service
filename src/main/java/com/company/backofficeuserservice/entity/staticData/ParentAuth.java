package com.company.backofficeuserservice.entity.staticData;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ParentAuth {

    @Id
    Short id;
    String name;
    boolean activity;
    LocalDateTime createDate;

    @PrePersist
    public void init(){
        createDate = LocalDateTime.now();
    }
}
