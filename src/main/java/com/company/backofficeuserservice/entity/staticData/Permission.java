package com.company.backofficeuserservice.entity.staticData;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "back_office")
@Setter
@Getter
public class Permission extends ParentAuth {

}
