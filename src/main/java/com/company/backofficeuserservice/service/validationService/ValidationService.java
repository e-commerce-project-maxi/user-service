package com.company.backofficeuserservice.service.validationService;

import java.io.Serializable;

public interface ValidationService {
    void verify(Serializable serializable);
}
