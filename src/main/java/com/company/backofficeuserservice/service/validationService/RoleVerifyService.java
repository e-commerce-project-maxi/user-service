package com.company.backofficeuserservice.service.validationService;

import com.company.backofficeuserservice.service.repoService.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import static com.company.backofficeuserservice.constant.CommonConstant.ValidationConstant.ROLE_VERIFY;

@Component(ROLE_VERIFY)
@RequiredArgsConstructor
public class RoleVerifyService implements ValidationService {

    private final RoleService roleService;

    @Override
    public void verify(Serializable roleId) {
        roleService.isExist((Short) roleId);
    }
}
