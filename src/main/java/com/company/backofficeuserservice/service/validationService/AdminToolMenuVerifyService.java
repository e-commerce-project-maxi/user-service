package com.company.backofficeuserservice.service.validationService;

import com.company.backofficeuserservice.service.repoService.AdminToolMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import static com.company.backofficeuserservice.constant.CommonConstant.ValidationConstant.ADMIN_TOOL_MENU_VERIFY;

@Component(ADMIN_TOOL_MENU_VERIFY)
@RequiredArgsConstructor
public class AdminToolMenuVerifyService  implements ValidationService{

    private final AdminToolMenuService adminToolMenuService;

    @Override
    public void verify(Serializable adminId) {
        adminToolMenuService.isExist((Integer) adminId);
    }
}
