package com.company.backofficeuserservice.service.validationService;

import com.company.backofficeuserservice.exception.NotFoundException;
import com.company.backofficeuserservice.service.repoService.BackUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import static com.company.backofficeuserservice.constant.CommonConstant.ValidationConstant.USER_VERIFY;
import static com.company.backofficeuserservice.enums.BusinessExceptionEnum.USER_NOT_FOUND;

@Component(USER_VERIFY)
@RequiredArgsConstructor
public class UserVerifyService implements ValidationService {

    private final BackUserService backUserService;

    @Override
    public void verify(Serializable userId) {
        if(!backUserService.existUserById((Long) userId)){
            throw new NotFoundException(USER_NOT_FOUND, userId);
        }
    }
}
