package com.company.backofficeuserservice.service.validationService;

import com.company.backofficeuserservice.dto.reuqest.validationDto.ValidationRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import static com.company.backofficeuserservice.constant.CommonConstant.ValidationConstant.*;

@Component
@RequiredArgsConstructor
public class ValidationFactory {

    @Qualifier(ADMIN_TOOL_MENU_VERIFY)
    private final ValidationService adminToolMenuVerifyService;
    @Qualifier(ROLE_VERIFY)
    private final ValidationService roleVerifyService;
    @Qualifier(USER_VERIFY)
    private final ValidationService userVerifyService;

    public void verifyUserMenu(ValidationRequest request) {
        if (!ObjectUtils.isEmpty(request.getAdminMenuId())) {
            adminToolMenuVerifyService.verify(request.getAdminMenuId());
        } else if (!ObjectUtils.isEmpty(request.getRoleId())) {
            roleVerifyService.verify(request.getRoleId());
        } else if (!ObjectUtils.isEmpty(request.getUserId())) {
            userVerifyService.verify(request.getUserId());
        }
    }
}
