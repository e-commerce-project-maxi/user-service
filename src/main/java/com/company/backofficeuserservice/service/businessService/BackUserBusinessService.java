package com.company.backofficeuserservice.service.businessService;

import com.company.backofficeuserservice.dto.response.user.BackUserResponse;
import com.company.backofficeuserservice.dto.reuqest.user.ChangePasswordRequest;
import com.company.backofficeuserservice.dto.reuqest.user.EmailRequest;
import com.company.backofficeuserservice.dto.reuqest.user.UserRequest;
import com.company.backofficeuserservice.enums.UserStatus;

import java.util.List;

public interface BackUserBusinessService {
    BackUserResponse createUser(UserRequest createRequest);

    BackUserResponse updateUser(Long id, UserRequest userRequest);

    BackUserResponse changePassword(ChangePasswordRequest changePasswordRequest);

    void updateUserRoleBaseAdminToolMenu(Long id, int adminMenuId, short roleId);

    void addAdminToolMenuAndRoleForUser(Long id,int adminMenuId, short roleId);

    String updateStatus(long id, UserStatus userStatus);

    String unBlockUser(String username);

    BackUserResponse findById(Long id);

    BackUserResponse findByUserName(String username);

    List<BackUserResponse> findAll(int page, int offset);

    String generatePassword(EmailRequest request);

    String checkEmailAlreadyExist(EmailRequest email);
}
