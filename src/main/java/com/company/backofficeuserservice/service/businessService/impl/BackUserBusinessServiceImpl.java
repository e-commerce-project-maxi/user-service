package com.company.backofficeuserservice.service.businessService.impl;

import com.company.backofficeuserservice.dto.response.user.BackUserResponse;
import com.company.backofficeuserservice.dto.reuqest.user.ChangePasswordRequest;
import com.company.backofficeuserservice.dto.reuqest.user.EmailRequest;
import com.company.backofficeuserservice.dto.reuqest.user.UserRequest;
import com.company.backofficeuserservice.dto.reuqest.validationDto.ValidationRequest;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.enums.UserStatus;
import com.company.backofficeuserservice.exception.PasswordException;
import com.company.backofficeuserservice.mapper.UserMapper;
import com.company.backofficeuserservice.service.businessService.BackUserBusinessService;
import com.company.backofficeuserservice.service.repoService.AccountAttemptService;
import com.company.backofficeuserservice.service.repoService.BackUserService;
import com.company.backofficeuserservice.service.validationService.ValidationFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.backofficeuserservice.enums.BusinessExceptionEnum.PASSWORD_NOT_MATCH;
import static com.company.backofficeuserservice.util.PasswordCryptChecking.hashPassword;
import static com.company.backofficeuserservice.util.PasswordCryptChecking.matchPassword;

@Service
@RequiredArgsConstructor
public class BackUserBusinessServiceImpl implements BackUserBusinessService {
    private final UserMapper userMapper;
    private final BackUserService backUserService;
    private final AccountAttemptService accountAttemptService;
    private final ValidationFactory validationFactory;

    @Override
    public BackUserResponse createUser(UserRequest createRequest) {
        return userMapper.toDto(
                backUserService.createUser(userMapper.toCreateEntity(createRequest))
        );
    }

    @Override
    public BackUserResponse updateUser(Long id, UserRequest userRequest) {
        BackUser backUser = backUserService.findById(id);
        return userMapper.toDto(
                backUserService.createUser(
                        userMapper.createInstance(backUser, userRequest))
        );
    }

    @Override
    public BackUserResponse changePassword(ChangePasswordRequest changePasswordRequest) {
        BackUser backUser = backUserService.findByUserName(changePasswordRequest.getUsername());
        if (matchPassword(changePasswordRequest.getOldPassword(), backUser.getPassword())) {
            backUser.setPassword(hashPassword(changePasswordRequest.getNewPassword()));
            return userMapper.toDto(backUserService.createUser(backUser));
        }
        throw new PasswordException(PASSWORD_NOT_MATCH);
    }

    @Override
    public void updateUserRoleBaseAdminToolMenu(Long id, int adminMenuId, short roleId) {
        validationFactory.verifyUserMenu(ValidationRequest.builder()
                .adminMenuId(adminMenuId)
                .roleId(roleId)
                .build());
        backUserService.updateUserRoleBaseAdminToolMenu(id, adminMenuId, roleId);
    }

    @Override
    public void addAdminToolMenuAndRoleForUser(Long id, int adminMenuId, short roleId) {
        validationFactory.verifyUserMenu(ValidationRequest.builder()
                .adminMenuId(adminMenuId)
                .roleId(roleId)
                .build());
        backUserService.addAdminToolMenuAndRoleForUser(id, adminMenuId, roleId);
    }


    @Override
    public String updateStatus(long id, UserStatus userStatus) {
        validationFactory.verifyUserMenu(ValidationRequest.builder().userId(id).build());
        return backUserService.updateStatus(id, userStatus);
    }

    @Override
    public String unBlockUser(String username) {
        backUserService.updateStatusByUsername(username);
        return accountAttemptService.unBlockUser(username);
    }

    @Override
    public BackUserResponse findById(Long id) {
        return userMapper.toDto(backUserService.findById(id));
    }

    @Override
    public BackUserResponse findByUserName(String username) {
        return userMapper.toDto(backUserService.findByUserName(username));
    }

    @Override
    public List<BackUserResponse> findAll(int page, int offset) {
        return backUserService.findAll(page, offset)
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public String generatePassword(EmailRequest request) {
        return backUserService.generatePassword(request);
    }

    @Override
    public String checkEmailAlreadyExist(EmailRequest email) {
        return backUserService.checkEmailAlreadyExist(email);
    }
}
