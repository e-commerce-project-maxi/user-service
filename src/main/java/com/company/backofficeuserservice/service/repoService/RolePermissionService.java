package com.company.backofficeuserservice.service.repoService;

import com.company.backofficeuserservice.entity.RolePermission;

public interface RolePermissionService {

    RolePermission getDefaultRolePermission();
    RolePermission getRolePermissionById(Short id);
}
