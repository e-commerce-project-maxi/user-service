package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.entity.RolePermission;
import com.company.backofficeuserservice.entity.UserMenuJoin;
import com.company.backofficeuserservice.entity.staticData.Role;
import com.company.backofficeuserservice.repository.UserMenuJoinRepository;
import com.company.backofficeuserservice.service.repoService.BackUserService;
import com.company.backofficeuserservice.service.repoService.UserMenuJoinService;
import com.company.backofficeuserservice.util.UserMenuJoinInstance;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserMenuJoinServiceImpl implements UserMenuJoinService {

    private final UserMenuJoinRepository userMenuJoinRepository;
    private final UserMenuJoinInstance userMenuJoinInstance;

    @Transactional
    @Override
    public void createUserMenuJoin(BackUser backUser, AdminToolMenu adminToolMenu, Role role) {
        userMenuJoinRepository.save(userMenuJoinInstance.createInstance(backUser, adminToolMenu, role));
    }

    @Override
    public void updateUserRoleBaseAdminToolMenu(Long userId, int menuId, Role rolePermission) {
        UserMenuJoin userMenuJoin = userMenuJoinRepository.findUserMenuJoinByAdminToolMenu_IdAndBackUser_Id(menuId, userId);
        userMenuJoin.setRole(rolePermission);
        userMenuJoinRepository.save(userMenuJoin);
    }

    @Override
    public void addAdminToolMenuAndRoleForUser(BackUser backUser, AdminToolMenu adminToolMenu, Role rolePermission) {
        userMenuJoinRepository.save(userMenuJoinInstance.createInstance(backUser, adminToolMenu, rolePermission));
    }
}
