package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.dto.reuqest.user.EmailRequest;
import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.entity.staticData.Role;
import com.company.backofficeuserservice.enums.UserStatus;
import com.company.backofficeuserservice.exception.NotFoundException;
import com.company.backofficeuserservice.repository.UserRepository;
import com.company.backofficeuserservice.service.repoService.*;
import com.company.backofficeuserservice.util.PasswordGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.company.backofficeuserservice.constant.CommonConstant.CONTINUE_OPERATION;
import static com.company.backofficeuserservice.constant.CommonConstant.SUCCESS;
import static com.company.backofficeuserservice.enums.BusinessExceptionEnum.*;
import static com.company.backofficeuserservice.util.PasswordCryptChecking.hashPassword;

@Slf4j
@Service
@RequiredArgsConstructor
public class BackUserServiceImpl implements BackUserService {

    private final UserRepository userRepository;
    private final UserMenuJoinService userMenuJoinService;
    public final AdminToolMenuService adminToolMenuService;
    public final RoleService roleService;
    private final SendEmailService sendEmailService;

    @Override
    @Transactional
    public BackUser createUser(BackUser backUser) {
        backUser = userRepository.save(backUser);
        AdminToolMenu defaultAdminTool = adminToolMenuService.getDefaultToolMenu();
        Role role = roleService.getDefaultRole();
        userMenuJoinService.createUserMenuJoin(backUser, defaultAdminTool, role);
        return backUser;
    }

    @Override
    public void updateUserRoleBaseAdminToolMenu(Long id, int adminMenuId, short roleId) {
        Role role = roleService.getById(roleId);
        userMenuJoinService.updateUserRoleBaseAdminToolMenu(id, adminMenuId, role);
    }

    @Override
    public void addAdminToolMenuAndRoleForUser(Long id, int adminMenuId, short roleId) {
        AdminToolMenu adminToolMenu = adminToolMenuService.getToolMenuById(adminMenuId);
        Role role = roleService.getById(roleId);
        Optional<BackUser> backUser = userRepository.findById(id);
        userMenuJoinService.addAdminToolMenuAndRoleForUser(backUser.get(), adminToolMenu, role);
    }

    @Override
    public String updateStatus(long id, UserStatus userStatus) {
        Optional<BackUser> backUser = userRepository.findById(id);
        if (backUser.isPresent()) {
            BackUser user = backUser.get();
            user.setStatus(userStatus);
            userRepository.save(user);
            return SUCCESS;
        }
        return "ERROR";

    }

    @Override
    public String updateStatusByUsername(String username) {
        Optional<BackUser> optionalBackUser = userRepository.findBackUserByUsername(username);
        if (optionalBackUser.isPresent()) {
            BackUser backUser = optionalBackUser.get();
            backUser.setStatus(UserStatus.ACTIVE);
            backUser.setAccountBlock(false);
            userRepository.save(backUser);
        }
        return SUCCESS;
    }

    @Override
    public BackUser findById(Long id) {
        Optional<BackUser> optionalBackUser = userRepository.findById(id);
        if (optionalBackUser.isPresent()) {
            return optionalBackUser.get();
        }
        throw new NotFoundException(USER_NOT_FOUND, id);
    }

    @Override
    public BackUser findByUserName(String username) {
        Optional<BackUser> optionalBackUser = userRepository.findBackUserByUsername(username);
        if (optionalBackUser.isPresent()) {
            return optionalBackUser.get();
        }
        throw new NotFoundException(USER_NOT_FOUND_BY_USERNAME, username);
    }

    @Override
    public List<BackUser> findAll(int page, int size) {
        return userRepository.findAll(PageRequest.of(page, size)).toList();
    }

    @Override
    public String generatePassword(EmailRequest request) {
        Optional<BackUser> optionalBackUser = userRepository.findBackUserByEmail(request.getEmail());
        if (optionalBackUser.isPresent()) {
            String generatedPassword = PasswordGenerator.generatePassword();
            changePassword(generatedPassword, optionalBackUser.get());
            return sendEmailService.sendGeneratedPassword(request.getEmail(), generatedPassword);
        }
        throw new NotFoundException(USER_NOT_FOUND_BY_EMAIL, request.getEmail());
    }

    @Override
    public String checkEmailAlreadyExist(EmailRequest email) {
        if (userRepository.existsBackUserByEmail(email.getEmail())) {
            throw new NotFoundException(EMAIL_ALREADY_EXIST, email);
        }
        return CONTINUE_OPERATION;
    }

    @Override
    public boolean existUserById(Long id) {
        return userRepository.existsBackUserById(id);
    }

    private void changePassword(String password, BackUser backUser) {
        backUser.setPassword(hashPassword(password));
        userRepository.save(backUser);
    }
}
