package com.company.backofficeuserservice.service.repoService;

public interface SendEmailService {

    String sendGeneratedPassword(String emailRecipient,String password);
}
