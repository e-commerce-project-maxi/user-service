package com.company.backofficeuserservice.service.repoService;

import com.company.backofficeuserservice.entity.staticData.Role;

public interface RoleService {

    void isExist(Short roleId);

    Role getDefaultRole();

    Role getById(Short roleId);
}
