package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.exception.NotFoundException;
import com.company.backofficeuserservice.service.repoService.SendEmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static com.company.backofficeuserservice.constant.CommonConstant.EmailConstant.*;
import static com.company.backofficeuserservice.enums.BusinessExceptionEnum.MESSAGE_ERROR;

@Slf4j
@Service
@RequiredArgsConstructor
public class SendEmailServiceImpl implements SendEmailService {

    @Value("${company.gmail.username}")
    private String fromMail;
    private final JavaMailSender javaMailSender;

    @Override
    public String sendGeneratedPassword(String emailRecipient, String password) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(new InternetAddress(fromMail));
            mimeMessageHelper.setTo(emailRecipient);
            mimeMessageHelper.setSubject(SUBJECT_LINE);
            mimeMessageHelper.setText(getMessageText(password));
            javaMailSender.send(mimeMessage);
            log.info("Mail with attachment sent successfully..");
            return MAIL_MESSAGE;
        } catch (MessagingException mex) {
            log.error("Message error: {}", mex.getMessage());
            throw new NotFoundException(MESSAGE_ERROR);
        }
    }

    private String getMessageText(String password) {
        return String.format(MESSAGE_TEXT, password);
    }
}
