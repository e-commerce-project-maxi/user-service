package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.entity.staticData.Role;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;
import com.company.backofficeuserservice.exception.NotFoundException;
import com.company.backofficeuserservice.repository.RoleRepository;
import com.company.backofficeuserservice.service.repoService.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Override
    public void isExist(Short roleId) {
        if (!repository.existsRoleById(roleId)) {
            throw new NotFoundException(BusinessExceptionEnum.ROLE_NOT_FOUND, roleId);
        }
    }

    @Override
    public Role getDefaultRole() {
        return repository.getById((short) 1);
    }

    @Override
    public Role getById(Short roleId) {
        Optional<Role> optionalRole = repository.findById(roleId);
        return optionalRole.orElseThrow(() -> new NotFoundException(BusinessExceptionEnum.ROLE_NOT_FOUND, roleId));
    }
}
