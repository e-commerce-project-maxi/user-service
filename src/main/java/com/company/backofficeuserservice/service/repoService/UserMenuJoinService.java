package com.company.backofficeuserservice.service.repoService;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.entity.staticData.Role;

public interface UserMenuJoinService {
    void createUserMenuJoin(BackUser backUser, AdminToolMenu adminToolMenu, Role role);

    void updateUserRoleBaseAdminToolMenu(Long userId, int menuId, Role role);

    void addAdminToolMenuAndRoleForUser(BackUser backUser, AdminToolMenu adminToolMenu, Role role);
}
