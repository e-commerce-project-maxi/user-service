package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.entity.AccountAttempt;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.enums.Status;
import com.company.backofficeuserservice.repository.AccountAttemptRepository;
import com.company.backofficeuserservice.repository.UserRepository;
import com.company.backofficeuserservice.service.repoService.AccountAttemptService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.company.backofficeuserservice.constant.CommonConstant.SUCCESS;

@Service
@RequiredArgsConstructor
public class AccountAttemptServiceImpl implements AccountAttemptService {

    private final AccountAttemptRepository accountAttemptRepository;
    private final UserRepository userRepository;

    @Override
    public String unBlockUser(String username) {
        AccountAttempt accountAttempt = accountAttemptRepository.findByUsername(username);
        if (!ObjectUtils.isEmpty(accountAttempt)) {
            accountAttempt.setStatus(Status.ACTIVE);
            accountAttempt.setModifiedTime(LocalDateTime.now());
            accountAttempt.setAttemptCount(0);
            accountAttemptRepository.save(accountAttempt);
            return SUCCESS;
        } else {
            Optional<BackUser> backUser = userRepository.findBackUserByUsername(username);
            if (backUser.isPresent()) {
                accountAttempt = new AccountAttempt();
                accountAttempt.setStatus(Status.ACTIVE);
                accountAttempt.setUsername(username);
                accountAttempt.setCreationTime(LocalDateTime.now());
                accountAttempt.setUserId(backUser.get().getId());
                accountAttempt.setAttemptCount(0);
                accountAttemptRepository.save(accountAttempt);
                return SUCCESS;
            }
        }
        return "ERROR";
    }
}
