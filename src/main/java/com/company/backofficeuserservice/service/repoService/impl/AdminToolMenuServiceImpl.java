package com.company.backofficeuserservice.service.repoService.impl;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import com.company.backofficeuserservice.enums.BusinessExceptionEnum;
import com.company.backofficeuserservice.exception.NotFoundException;
import com.company.backofficeuserservice.repository.AdminToolMenuRepository;
import com.company.backofficeuserservice.service.repoService.AdminToolMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdminToolMenuServiceImpl implements AdminToolMenuService {

    private final AdminToolMenuRepository adminToolMenuRepository;

    @Override
    public void isExist(Integer adminToolMenuId) {
        if (!adminToolMenuRepository.existsAdminToolMenuById(adminToolMenuId)) {
            throw new NotFoundException(BusinessExceptionEnum.ADMIN_TOOL_MENU_NOT_FOUND, adminToolMenuId);
        }
    }

    @Override
    public AdminToolMenu getDefaultToolMenu() {
        return adminToolMenuRepository.getById(1);
    }

    @Override
    public AdminToolMenu getToolMenuById(Integer id) {
        return adminToolMenuRepository.getById(id);
    }
}
