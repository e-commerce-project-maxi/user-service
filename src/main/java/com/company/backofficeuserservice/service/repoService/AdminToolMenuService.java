package com.company.backofficeuserservice.service.repoService;

import com.company.backofficeuserservice.entity.AdminToolMenu;
import io.swagger.models.auth.In;

public interface AdminToolMenuService {

    void isExist(Integer adminToolMenuId);

    AdminToolMenu getDefaultToolMenu();
    AdminToolMenu getToolMenuById(Integer id);


}
