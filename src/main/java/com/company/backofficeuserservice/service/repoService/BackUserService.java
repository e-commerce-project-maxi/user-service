package com.company.backofficeuserservice.service.repoService;

import com.company.backofficeuserservice.dto.reuqest.user.EmailRequest;
import com.company.backofficeuserservice.entity.BackUser;
import com.company.backofficeuserservice.enums.UserStatus;

import java.util.List;

public interface BackUserService {
    BackUser createUser(BackUser backUser);

    void updateUserRoleBaseAdminToolMenu(Long id, int adminMenuId, short roleId);

    void addAdminToolMenuAndRoleForUser(Long id, int adminMenuId, short roleId);

    String updateStatus(long id, UserStatus userStatus);

    String updateStatusByUsername(String username);

    BackUser findById(Long id);

    BackUser findByUserName(String username);

    List<BackUser> findAll(int page, int offset);

    String generatePassword(EmailRequest request);

    String checkEmailAlreadyExist(EmailRequest email);

    boolean existUserById(Long id);
}
