package com.company.backofficeuserservice.converter;

import com.company.backofficeuserservice.exception.TokenNotValidException;
import com.company.backofficeuserservice.tk.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.company.backofficeuserservice.constant.CommonConstant.SECURITY_HEADER;
import static com.company.backofficeuserservice.enums.BusinessExceptionEnum.TOKEN_NOT_VALID;

@Component
@RequiredArgsConstructor
public class RequestDtoConverter {
    private final HttpServletRequest httpServletRequest;
    private final JwtTokenUtil jwtTokenUtil;


    public void checkAccess(){

    }

    public String getUsernameFromToken() {
        String token = httpServletRequest.getHeader(SECURITY_HEADER);
        if (token != null && token.startsWith("Bearer")) {
            token = token.replace("Bearer ", "");
            try {
                return jwtTokenUtil.getUsernameFromToken(token);
            } catch (Exception e) {
                throw new TokenNotValidException(TOKEN_NOT_VALID);
            }
        } else {
            return null;
        }
    }
}
